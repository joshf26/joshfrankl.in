var symbols = ["\u00E5","\u222B","\u00E7","\u2202","\u00B4","\u0192","\u00A9","\u02D9","\u02C6","\u2206","\u02DA","\u00AC","\u00B5","\u02DC","\u00F8","\u03C0","\u0153","\u00AE","\u00DF","\u2020","\u00A8","\u221A","\u2211","\u2248","\u00A5","\u03A9","\u00c5","\u0131","\u00c7","\u00ce","\u00b4","\u00cf","\u02dd","\u00d3","\u02c6","\u00d4","\uf8ff","\u00d2","\u00c2","\u02dc","\u00d8","\u220f","\u0152","\u2030","\u00cd","\u02c7","\u00a8","\u25ca","\u201e","\u02db","\u00c1","\u00b8","\u00a1","\u2122","\u00a3","\u00a2","\u221e","\u00a7","\u00b6","\u2022","\u00aa","\u00ba","\u2044","\u20ac","\u2039","\u203a","\ufb01","\ufb02","\u2021","\u00b0","\u00b7","\u201a","\u2264","\u2265","\u00f7","\u2026","\u00e6","\u201c","\u2018","\u00ab","\u2013","\u2260","\u00af","\u02d8","\u00bf","\u00da","\u00c6","\u201d","\u2019","\u00bb","\u2014","\u00b1"];
var letters = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0","!","@","#","$","%","^","&","*","(",")",",",".","/",";","'","[","]","\\","-","=","<",">","?",":",'"',"{","}","|","_","+"];
function convert(){
	var text = document.getElementById("input").value;
	var characters = text.split('');
	for (var i = 0; i < characters.length; i++){
		for (var j = 0; j < symbols.length; j++){
			if (characters[i] == symbols[j])
				characters[i] = letters[j];
		}
	}
	text = characters.join('');
	document.getElementById("output").innerHTML = text;
}
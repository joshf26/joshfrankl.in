var menu = document.getElementById("menu");
var blocked = document.getElementById("blocked");
var outOfData = document.getElementById("out_of_data");
var outOfMoney = document.getElementById("out_of_money");
var frame = document.getElementById("frame");
var dataCounter = document.getElementById("data_remaining");
var moneyCounter = document.getElementById("money_remaining");
var currentPlanDisplay = document.getElementById("current_plan");
var currentPlanLabel = document.getElementById("current_plan_label");
var changePlanButton = document.getElementById("change_plan");
var homeButton = document.getElementById("home_button");

var BASIC_DATA = 256;
var STANDARD_DATA = 1024;
var ULTRA_DATA = 2048;
var BASIC_PRICE = 35;
var STANDARD_PRICE = 75;
var ULTRA_PRICE = 100;

var money = 1000;
var data = BASIC_DATA;
var plan = "Basic";

var inMenu = true;
var noCharge = true;

var dataAnimationInterval;
var moneyAnimationInterval;

setInterval(function(){
    if (!inMenu && Math.random() < 0.1){
        useRandomData();
    }
}, 1000);

function changePlan(){
    currentPlanDisplay.innerText = plan;
    currentPlanLabel.style.display = "none";
    changePlanButton.style.display = "none";
    homeButton.style.display = "none";
    menu.style.display = "inline";
    frame.style.display = "none";
    inMenu = true;
}

function choosePlan(p){
    charge = false;
    switch (plan) {
        case "Basic":
            if (data != BASIC_DATA) charge = true;
            break;
        case "Standard":
            if (data != STANDARD_DATA) charge = true;
            break;
        case "Ultra":
            if (data != ULTRA_DATA) charge = true;
            break;
    }
    plan = p;
    switch (plan) {
        case "Basic":
            if (charge) pay(BASIC_PRICE);
            data = BASIC_DATA;
            break;
        case "Standard":
            if (charge) pay(STANDARD_PRICE);
            data = STANDARD_DATA;
            break;
        case "Ultra":
            if (charge) pay(ULTRA_PRICE);
            data = ULTRA_DATA;
            break;
    }
    if (money > 0){
        updateDataCounter();
        currentPlanDisplay.innerText = plan;
        currentPlanLabel.style.display = "inline";
        changePlanButton.style.display = "inline";
        homeButton.style.display = "inline";
        menu.style.display = "none";
        frame.style.display = "inline";
        inMenu = false;
    }
    else {

    }
}

function home() {
    frame.src = "https://start.duckduckgo.com/";
    frame.style.display = "inline";
    blocked.style.display = "none";
    inMenu = false;
}

function visitPage(){
    if (noCharge){
        noCharge = false;
        return;
    }

    if (frame.contentWindow.length == 0) blockPage();

    if (plan != "Ultra" && Math.random() < ((plan == "Standard") ? 0.05 : 0.1)){
        blockPage();
    }

    useData(Math.floor(Math.random() * 50) + 10);
}

function blockPage() {
    blocked.style.display = "block";
    frame.style.display = "none";
    inMenu = true;
}

function nextMonth(){
    switch (plan) {
        case "Basic":
            pay(BASIC_PRICE);
            data = BASIC_DATA;
            break;
        case "Standard":
            pay(STANDARD_PRICE);
            data = STANDARD_DATA;
            break;
        case "Ultra":
            pay(ULTRA_PRICE);
            data = ULTRA_DATA;
            break;
    }
    updateDataCounter();
    if (money > 0){
        frame.style.display = "inline";
        outOfData.style.display = "none";
        inMenu = false;
    }
}

function pay(target) {
    targetMoney = money - target;
    clearInterval(moneyAnimationInterval);
    moneyAnimationInterval = setInterval(function(){
        if (money == targetMoney){
            clearInterval(moneyAnimationInterval)
            if (money <= 0) {
                outOfMoney.style.display = "block";
                outOfData.style.display = "none";
                blocked.style.display = "none";
                frame.style.display = "none";
                menu.style.display = "none";
                inMenu = true;
            }
        } else {
            money -= 1;
            moneyCounter.innerText = "$" + money;
        }
    }, 20);
}

function useData(target){
    targetData = data - target;
    clearInterval(dataAnimationInterval);
    dataAnimationInterval = setInterval(function(){
        if (data == targetData){
            clearInterval(dataAnimationInterval)
            if (data <= 0){
                outOfData.style.display = "block";
                outOfMoney.style.display = "none";
                blocked.style.display = "none";
                frame.style.display = "none";
                menu.style.display = "none";
                inMenu = true;
            }
        } else {
            data -= 1;
            updateDataCounter();
        }
    }, 20);
}

function useRandomData(){
    useData(Math.floor(Math.random() * 10) + 1);
}

function updateDataCounter(){
    dataCounter.innerText = data + "MB";
}

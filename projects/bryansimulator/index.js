var percent = 0;
var auto = 0;
var click = 1;
var storyPoint = 0;
var counter = document.getElementById("counter");
window.setInterval(autoAdd, 100);

intervals = [
	1,
	10,
	25,
	50,
	75,
	100,
	110,
	125,
	150,
	200,
	300,
	500,
	1000,
	2000,
	50000,
	100000,
	2000000,
	5000000
]

story = [
	"Welcome to Bryan Simulator 2.0! Click the Big Bad Bryan Button to start!",
	"Congratulations! You have started your journey of becoming bryan.",
	"Wow, you're already a tenth of the way to becoming Bryan!",
	"Getting closer!",
	"Oh boy, halfway there!",
	"Almost!",
	"Hurray! You've become Bryan!",
	"Wait no! You can't go over 100%!",
	"This is not right. Please stop becoming Bryan.",
	"Really? You're already more than half of another Bryan.",
	"Okay you need to stop.",
	"This is just too much Bryan.",
	"I didn't even know anyone could be the bad!",
	"This is the equivalent of ten Bryans. What the heck are you doing?",
	"Can anybody possibly be this crispy?",
	"Your chin must be so sharp right now!",
	"This is just absurd. Won't you give up already?",
	"I have no words...",
	"My god..."
]

function clickButton(){
	percent += click;
	updateDisplay();
}

function buy(item){
	var cost = parseInt(document.getElementById("price-"+item).innerHTML);
	if (percent >= cost){
		percent -= cost;
		amount = parseInt(document.getElementById("amount-"+item).innerHTML);
		document.getElementById("amount-"+item).innerHTML = amount + 1;
		auto += parseInt(document.getElementById("percent-"+item).innerHTML);
		click += parseInt(document.getElementById("click-"+item).innerHTML);
		updateDisplay();
		document.getElementById("price-"+item).innerHTML = Math.round(cost * (Math.pow(1.1,(amount+1))));
	}
}

function autoAdd(){
	percent += auto/10;
	updateDisplay();
}

function updateDisplay(){
	counter.innerHTML = Math.round(percent) + "% Bryan";
	if (percent >= intervals[storyPoint]){
		storyPoint += 1;
		document.getElementById("story").innerHTML = story[storyPoint];
	}
}
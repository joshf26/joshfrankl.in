var config = {
	apiKey: "AIzaSyC5NoloAQieetfwRnT4ugqCeL4GX0D_XVk",
    authDomain: "172.16.54.176", // change this to "joshfranl.in"
    databaseURL: "https://whack-a-mobile-49ae9.firebaseio.com",
    projectId: "whack-a-mobile-49ae9",
    storageBucket: "whack-a-mobile-49ae9.appspot.com",
    messagingSenderId: "798665927451"
};
firebase.initializeApp(config);

document.getElementById("host").style.visibility = "hidden";
var clientKey;
var gameName;
var time;
var interval;
var players;
var intervalID;
var colors = [
	'red',
	'yellow',
	'green',
	'blue',
	'purple',
	'orange',
	'pink',
	'white'
]

function updateDispaly(){
	document.getElementById("playerDisplay").innerHTML = "Players: "+document.getElementById("playerSlider").value+" players.";
	document.getElementById("timeDisplay").innerHTML = "Time: "+document.getElementById("timeSlider").value+" seconds.";
	document.getElementById("intervalDisplay").innerHTML = "Interval: "+document.getElementById("intervalSlider").value+" second(s).";
}

function checkEmpty(s){
	if (s == ""){
		return true;
	}
	return false;
}

function createGame(){
	gameName = document.getElementById('game_name').value
	if (checkEmpty(gameName)){
		alert("Game name cannot be empty.");
		return;
	}

	var gameData = {};
	gameData[gameName] = {
		gameStarted: false,
		connectedClients: {}
	}

	firebase.database().ref('/games/'+gameName+'/').once('value').then(function(snapshot) {
		if (snapshot.val() == null){

			firebase.database().ref().child('games').update(gameData);

			document.getElementById("menu").style.visibility = "hidden";
			document.getElementById("host").style.visibility = "visible";
			document.getElementById("title").innerHTML = "Currently Hosting: " + gameName;
			document.getElementById("subtitle").innerHTML = "Phones Connected: 0";

			firebase.database().ref('games/'+gameName+'/connectedClients').on('value', function(snapshot) {
				if (snapshot.val() != null){
					document.getElementById("subtitle").innerHTML = "Phones Connected: "+snapshot.numChildren();
				}
			});

		}
		else {

			alert("That name has already been taken. Please try again.");

		}
	});

}

function joinGame(){
	gameName = document.getElementById('game_name').value
	if (checkEmpty(gameName)){
		alert("Game name cannot be empty.");
		return;
	}

	firebase.database().ref('/games/'+gameName+'/').once('value').then(function(snapshot) {
		if (snapshot.val() == null){

			alert("That games does not exist yet. Check your spelling, and make sure the game is being hosted.");

		}
		else {

			document.getElementById("menu").style.visibility = "hidden";
			document.getElementById("title").innerHTML = "Waiting for host to start...";
			document.getElementById("subtitle").innerHTML = "";
			document.getElementById("background").style.backgroundColor = "black";

			clientKey = firebase.database().ref().child('games/'+gameName+'/connectedClients').push({color:"black"}).key;

			firebase.database().ref('games/'+gameName+'/connectedClients/'+clientKey).on('value', function(snapshot) {
				document.getElementById("background").style.backgroundColor = snapshot.val().color;
			});
			firebase.database().ref('games/'+gameName+'/gameStarted/').on('value', function(snapshot) {
				if (snapshot.val()){
					document.getElementById("title").style.visibility = "hidden";
				}
				else {
					document.getElementById("title").style.visibility = "visible";
				}
			});

			document.ontouchstart = function(){

				console.log("wew");
				scoredColor = document.getElementById("background").style.backgroundColor;
				for (var i = 0; i < 7; i++){
					if (scoredColor == colors[i]){
						console.log("clicked "+colors[i]+ " -- "+ String(i));
						console.log('/players/player'+String(i+1)+'Score');
						var newI = i;
						firebase.database().ref('/games/'+gameName+'/players/player'+String(i+1)+'Score').once('value').then(function(snapshot){
							console.log("Getting here? "+snapshot.val());
							newData = {}
							newData['player'+String(newI+1)+'Score'] = snapshot.val()+1;
							firebase.database().ref('/games/'+gameName+'/players').update(newData);
						});
					}
				}
				document.getElementById("background").style.backgroundColor = "black";

			};

		}
	});
}

function startGame(){
	firebase.database().ref('/games/'+gameName+'/gameStarted').set(true);

	players = document.getElementById("playerSlider").value;
	time = document.getElementById("timeSlider").value;
	interval = document.getElementById("intervalSlider").value;

	firebase.database().ref('/games/'+gameName+'/players').update({
		player1Score: 0,
		player2Score: 0,
		player3Score: 0,
		player4Score: 0,
		player5Score: 0,
		player6Score: 0,
		player7Score: 0,
		player8Score: 0
	});

	intervalID = window.setInterval(function(){
		firebase.database().ref('/games/'+gameName+'/connectedClients').once('value').then(function(snapshot) {
			for (var currentClient in snapshot.val()){
				firebase.database().ref('/games/'+gameName+'/connectedClients/'+currentClient+'/color').set(colors[Math.floor((Math.random() * players))]);
			}
		});
		time--;
		if (time == 0){
			// need to go to scores screen
			back();
		}
	}, interval*1000);
}

function back(){
	document.getElementById("menu").style.visibility = "visible";
	document.getElementById("host").style.visibility = "hidden";
	document.getElementById("title").innerHTML = "Whach-A-Mobile (BETA)";
	document.getElementById("subtitle").innerHTML = 'A game by <a href="http://jofrankl.in/">Joshua Franklin</a>';
	document.getElementById("background").style.backgroundColor = "red";
	clearInterval(intervalID);
}
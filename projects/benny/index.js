imageElement = document.getElementById("image");
textElement = document.getElementById("text");

function generate(){
	imageElement.src = choose(comics);
	text = "";
	for (x = 0; x < (Math.floor(Math.random() * 5) + 3); x++){
		text = text + " " + choose(lines);
	}
	textElement.innerText = text;
}

function choose(choices) {
	var index = Math.floor(Math.random() * choices.length);
	return choices[index];
}

generate();
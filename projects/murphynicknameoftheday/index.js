var prefixs = ["sm","m","m"];
var vowels = ["a","e","i","o","u","y"];
var sound1 = ["p","r","s","t","c"];
var sound2 = ["p","s","sh","pe","a","qu","ph"];
var endings = ["y","r","i"];
var full = [prefixs, vowels, sound1, sound2, endings];
var historyLength = 0;
var historyContainer = document.getElementById("historyContainer");
var currentDate = new Date();

function choose(choices, index, date) {
	Math.seedrandom((date.getFullYear()*100)+(date.getMonth()*1000)+date.getDate()+(index*10000));
	var index = Math.floor(Math.random() * choices.length);
	return choices[index];
}

function generate(date){
	output = "";
	for (currentList in full){
		output += choose(full[currentList], currentList, date);
	}
	return output;
}

function generateHistory(){
	historyLength += 5;
	for (i = historyLength; i < historyLength+5; i++){
		currentDate.setDate(currentDate.getDate() - 1);
		currentElement = document.createElement('p');
		currentText = document.createTextNode((currentDate.getMonth()+1)+"/"+currentDate.getDate()+"/"+currentDate.getFullYear() + " - " + generate(currentDate)); 
  		currentElement.appendChild(currentText);
  		historyContainer.appendChild(currentElement);
	}
}

function update() {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        generateHistory();
        return true;
    }
    return false;
};

function resize(){
	while (update()){}
}

window.onresize = resize;
window.onscroll = update;

todaysDate = new Date();
document.getElementById("header").innerText = "Today ("+(todaysDate.getMonth()+1)+"/"+todaysDate.getDate()+"/"+todaysDate.getFullYear()+"), Murphy's nickname is...";
document.getElementById("output").innerText = generate(todaysDate);
generateHistory();
resize();

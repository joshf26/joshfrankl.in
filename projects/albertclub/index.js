var config = {
	apiKey: "AIzaSyB1beBN7rtRWZzY6OcLba1m3w2aQ6yEzd4",
	authDomain: "albert-club.firebaseapp.com",
	databaseURL: "https://albert-club.firebaseio.com",
	storageBucket: "albert-club.appspot.com",
	messagingSenderId: "269167152941"
};
firebase.initializeApp(config);
var database = firebase.database();

if (!document.cookie.includes("userId")) {
	document.getElementById("signup").style.display = "block";
}
else {
	setWelcome(getUserId());
	document.getElementById("main").style.display = "block";
}

updateScrollBox();

var timerSeconds = Math.floor(Math.random() * 59);
var timerMinutes = Math.floor(Math.random() * 59);
var timerHours = Math.floor((Math.random() * 1000) + 200); 
window.setInterval(function(){
	timerSeconds--;
	if (timerSeconds == -1){
		timerSeconds = 59;
		timerMinutes--;
		if (timerMinutes == -1){
			timerMinutes = 59;
			timerHours--;
		}
	}
	var timerSecondsString;
	var timerMinutesString;
	if (timerSeconds < 10){
		timerSecondsString = "0" + timerSeconds;
	}
	else {
		timerSecondsString = "" + timerSeconds;
	}
	if (timerMinutes < 10){
		timerMinutesString = "0" + timerMinutes;
	}
	else {
		timerMinutesString = "" + timerMinutes;
	}
	document.getElementById("timer").style.color = "#"+((1<<24)*Math.random()|0).toString(16);
	document.getElementById("timer").innerText = timerHours + ":" + timerMinutesString + ":" + timerSecondsString;
}, 1000)

function join(){
	userId = Math.floor(Math.random()*1000000000);
	document.cookie = "userId=" + userId + "; expires=Wed, 1 Jan 3000 12:00:00 UTC; path=/projects/albertclub";
	firebase.database().ref('members/' + userId).set({
		name: document.getElementById("name").value
	});
	location.reload();
}

function getUserId(){
	return document.cookie.split("userId=")[1].split(";")[0];
}

function updateScrollBox(){
	firebase.database().ref('members').once('value').then(function(snapshot) {
		for (currentMemeber in snapshot.val()){
			listItem = document.createElement("li");
			listItem.id = "listItem";
			listItem.appendChild(document.createTextNode(snapshot.val()[currentMemeber].name));
			document.getElementById("scrollbox").appendChild(listItem);
		}
	});
}

function setWelcome(userId){
	firebase.database().ref('members/' + userId).once('value').then(function(snapshot) {
		var user = snapshot.val();
		if (user == null){
			document.cookie = "userId=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/projects/albertclub";
			location.reload();
		}
		document.getElementById("welcome").innerText = "Welcome to Albert Club, " + user.name + ".";
	});
}
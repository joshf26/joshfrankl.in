function generate() {
    var openers = [
        "You have lethal! ",
        "It's lethal! ",
        "Comeback time. ",
        "Don't concede yet! ",
        "You got this. ",
        "This is what you need to do. ",
        "Ready? ",
        "Here we go. ",
        "Let's go! "
    ];
    var closers = [
        "Easy game. ",
        "GG! ",
        "Winner! ",
        "Done. ",
        "EzPz. ",
        "Good game, well played."
    ];
    var transitions = [
        "Then, ",
        "Next, ",
        "Now, ",
        "Next turn, ",
        "At the end of your turn, ",
        "At the start of your next turn, "
    ];
    var characters = [
        "face",
        "hero",
        "weapon",
        "1-1",
        "4-3",
        "2-1",
        "shredder",
        "golem",
        "Dr. Boom",
        "Archmage Antonidas",
        "totem",
        "murloc",
        "owl",
        "juggler",
        "yeti",
        "Emperor Thaurissan",
        "imp",
        "demon",
        "pirate",
        "silver hand recruit",
        "magma rager"
    ];
    var actions = [
        "fireball",
        "frostbolt",
        "backstab",
        "silence",
        "flamestrike"
    ];
    var triggers = [
        "battlecry",
        "deathrattle",
        "inspire"
    ];
    var buffs = [
        "taunt",
        "windfury",
        "stealth",
        "spell damage",
        "blessing of kings",
        "valen's chosen",
        "nightmare"
    ];
    var sentences = [
        "attack your CHARACTER into their CHARACTER. ",
        "give your CHARACTER BUFF and trade into his CHARACTER. ",
        "you might want to ACTION his CHARACTER to clear up the board. ",
        "trade your CHARACTER into his CHARACTER. ",
        "trigger your CHARACTER's TRIGGER by ACTIONing his CHARACTER. ",
        "coin out your CHARACTER because its TRIGGER is very strong. ",
        "make sure to kill his CHARACTER, it's TRIGGER will destroy you. ",
        "in case he ACTIONs your CHARACTER, have a ACTION ready for next turn. ",
        "try to draw into ACTION, it'll hopefully counter his CHARACTER. ",
        "if he plays his CHARACTER, you can ACTION it. ",
        "play your CHARACTER, it's new meta! ",
        "if you want to BM, give your CHARACTER BUFF before ACTIONing his CHARACTER. "
    ];
    var transitionsAllowed = false;
    var output = "";
    var count = document.getElementById("length").value;
    if (count > -2) {
        output += openers[Math.floor(Math.random() * openers.length)];
    }
    for (i = 0; i < count; i++) {
        var currentSentence = sentences[Math.floor(Math.random() * sentences.length)];
        currentSentence = currentSentence.replace("ACTION", actions[Math.floor(Math.random() * actions.length)]);
        currentSentence = currentSentence.replace("ACTION", actions[Math.floor(Math.random() * actions.length)]);
        currentSentence = currentSentence.replace("CHARACTER", characters[Math.floor(Math.random() * characters.length)]);
        currentSentence = currentSentence.replace("CHARACTER", characters[Math.floor(Math.random() * characters.length)]);
        currentSentence = currentSentence.replace("CHARACTER", characters[Math.floor(Math.random() * characters.length)]);
        currentSentence = currentSentence.replace("BUFF", buffs[Math.floor(Math.random() * buffs.length)]);
        currentSentence = currentSentence.replace("TRIGGER", triggers[Math.floor(Math.random() * triggers.length)]);
        if (transitionsAllowed && Math.random() < 0.5) {
            currentSentence = transitions[Math.floor(Math.random() * transitions.length)] + currentSentence;
        } else {
            currentSentence = currentSentence.charAt(0).toUpperCase() + currentSentence.slice(1);
        }
        output += currentSentence;
        transitionsAllowed = true;
    }
    if (count > -1) {
        output += closers[Math.floor(Math.random() * closers.length)];
    }
    document.getElementById("output").innerHTML = output;
}
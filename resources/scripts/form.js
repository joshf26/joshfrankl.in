$('.carousel.carousel-slider').carousel({full_width: true});

$(document).ready(function(){
	$('.slider').slider({full_width: true});
});

setTimeout(function(){
	window.scrollTo(0, 0);
}, 1000);

$('#contact').one('submit',function(){
	var inputName = 'entry.1110670768=' + encodeURIComponent($('#clientName').val());
	var inputEmail = '&entry.327117419=' + encodeURIComponent($('#clientEmail').val());
	var inputReason = '&entry.1969777916=' + encodeURIComponent($('#clientReason').val());
	var submitRef = '&submit=Submit';
	var url = 'https://docs.google.com/forms/d/e/1FAIpQLSc6x5JunZDH0jRL8m9Ixqs9sRbjxnRlyFwcnl4_hODH-KKbzg/formResponse?'+inputName+inputEmail+inputReason+submitRef;

	$(this)[0].action=url;
	$('#contact').addClass('active').val('Thank You!');

	$(this).animate({
		height: "0px"
	});
	$('#thankyou').css({
		visibility: "visible"
	});
});